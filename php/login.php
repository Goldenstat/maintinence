$submit = $_POST['submit'];

if(!isset($submit))
{
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Bushbabies - Maintenance</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/signin.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="container">

    <div id="error"></div>

    <form class="form-signin" id="loginForm" method="post">
        <h2 class="form-signin-heading">Please sign in</h2>
        <label for="inputUser" class="sr-only">Username</label>
        <input type="text" name="username" id="inputUser" class="form-control" placeholder="Username" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>

        <div class="checkbox">
            <label>
                <input type="checkbox" value="remember-me"> Remember me
            </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" id="login" type="submit" name="submit">Sign in</button>
    </form>

</div>
<!-- /container -->

<script src="js/jquery.min.js"></script>
<script src="js/login.js"></script>
</body>
</html>
<?php
    } else {
    $username = $_POST['username'];
    $password = sha1($_POST['password']);

    try {
        $query = $db->prepare("SELECT `username`, `password` FROM `users` WHERE `username` = :uname AND `password` = :pword");

        $query->bindParam(":uname", $username);
        $query->bindParam(":pword", $password);

        $query->execute();

        $rows = $query->fetchObject();
        $count = $query->rowCount();

        if($count == 0)
        {
            echo 'Wrong Information.';
        } else {
            $_SESSION['username'] = $rows->username;
            header("Location: index.php");
        }
    } catch(PDOException $e) {
        die('Error: ' . $e);
    }



}