<?php
    ob_start();
    session_start();
    include('php/config.php');

    if(isset($_SESSION['username']))
    {
        require('php/header.php');
        require('php/main.php');
        require('php/footer.php');
    } else {
        require('php/login.php');
    }

    ?>